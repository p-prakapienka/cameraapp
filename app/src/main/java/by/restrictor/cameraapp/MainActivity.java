package by.restrictor.cameraapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.xml.datatype.Duration;

public class MainActivity extends AppCompatActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final String STATE_PHOTO_PATH = "photoPath";

    ImageView mImageView;

    String mPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button takePhotoButton = (Button) findViewById(R.id.call_camera_button);
        takePhotoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPhotoPath = null;
                dispatchTakePictureIntent();
            }
        });

        if (savedInstanceState != null) {
            mPhotoPath = savedInstanceState.getString(STATE_PHOTO_PATH);
        }
        mImageView = (ImageView) findViewById(R.id.iv_show_photo);

        if (mPhotoPath != null) {
            updateImageView();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_PHOTO_PATH, mPhotoPath);

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            updateImageView();
        }
    }

    private void updateImageView() {
        mImageView.setImageURI(FileProvider.getUriForFile(
                this, "by.restrictor.cameraapp.fileprovider", new File(mPhotoPath)));
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File image = null;
            try {
                image = getTempFile();
            } catch (IOException e) {
                Log.w("Error getting file: ", e);
                Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
            }
            if (image != null) {
                takePictureIntent.putExtra(
                        MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(this, "by.restrictor.cameraapp.fileprovider", image));
            }
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private File getTempFile() throws IOException {
        String fileName = "pa_image_" + new Date().getTime();
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                fileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        mPhotoPath = image.getAbsolutePath();
        return image;
    }
}
